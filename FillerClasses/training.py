from datetime import date as some_date
from decimal import Decimal as dec
import csv


film_1 = {
    'title': 'The Hobbit: An Unexpected Journey',
    'article_size': 25000,
    'producer': ['Peter Jackson', 'Fran Walsh', 'Zane Weiner'],
    'release_date': some_date(2013, 11, 28),
    'running_time': 169,
    'country': ['New Zealand', 'UK', 'USA'],
    'budget': dec('200000000')
}

film_2 = {
    'title': 'The Hobbit: An Unexpected Journey',
    'article_size': 25000,
    'producer': ['Peter Jackson', 'Fran Walsh', 'Zane Weiner'],
    'release_date': some_date(2013, 11, 28),
    'running_time': 169,
    'country': ['New Zealand', 'UK', 'USA'],
    'budget': dec('200000000')
}

film_3 = {
    'title': 'The Hobbit: An Unexpected Journey',
    'article_size': 25000,
    'producer': ['Peter Jackson', 'Fran Walsh', 'Zane Weiner'],
    'release_date': some_date(2013, 11, 28),
    'running_time': 169,
    'country': ['New Zealand', 'UK', 'USA'],
    'budget': dec('200000000')
}

film_4 = {
    'title': 'The Hobbit: An Unexpected Journey',
    'article_size': 25000,
    'producer': ['Peter Jackson', 'Fran Walsh', 'Zane Weiner'],
    'release_date': some_date(2013, 11, 28),
    'running_time': 169,
    'country': ['New Zealand', 'UK', 'USA'],
    'budget': dec('200000000')
}

film_5 = {
    'title': 'The Hobbit: An Unexpected Journey',
    'article_size': 25000,
    'producer': ['Peter Jackson', 'Fran Walsh', 'Zane Weiner'],
    'release_date': some_date(2013, 11, 28),
    'running_time': 169,
    'country': ['New Zealand', 'UK', 'USA'],
    'budget': dec('200000000')
}

film_6 = {
    'title': 'The Hobbit: An Unexpected Journey',
    'article_size': 25000,
    'producer': ['Peter Jackson', 'Fran Walsh', 'Zane Weiner'],
    'release_date': some_date(2013, 11, 28),
    'running_time': 169,
    'country': ['New Zealand', 'UK', 'USA'],
    'budget': dec('200000000')
}

film_7 = {
    'title': 'The Hobbit: An Unexpected Journey',
    'article_size': 25000,
    'producer': ['Peter Jackson', 'Fran Walsh', 'Zane Weiner'],
    'release_date': some_date(2013, 11, 28),
    'running_time': 169,
    'country': ['New Zealand', 'UK', 'USA'],
    'budget': dec('200000000')
}

film_8 = {
    'title': 'The Hobbit: An Unexpected Journey',
    'article_size': 25000,
    'producer': ['Peter Jackson', 'Fran Walsh', 'Zane Weiner'],
    'release_date': some_date(2013, 11, 28),
    'running_time': 169,
    'country': ['New Zealand', 'UK', 'USA'],
    'budget': dec('200000000')
}

film_9 = {
    'title': 'The Hobbit: An Unexpected Journey',
    'article_size': 25000,
    'producer': ['Peter Jackson', 'Fran Walsh', 'Zane Weiner'],
    'release_date': some_date(2013, 11, 28),
    'running_time': 169,
    'country': ['New Zealand', 'UK', 'USA'],
    'budget': dec('200000000')
}

film_10 = {
    'title': 'The Hobbit: An Unexpected Journey',
    'article_size': 25000,
    'producer': ['Peter Jackson', 'Fran Walsh', 'Zane Weiner'],
    'release_date': some_date(2013, 11, 28),
    'running_time': 169,
    'country': ['New Zealand', 'UK', 'USA'],
    'budget': dec('200000000')
}

films = [film_1,film_2,film_3,film_4,film_5,film_6,film_7,film_8,film_9,film_10]

"""
with open('file.csv', 'wb') as csvfile:
    bb = csv.writer(csvfile, quotechar=' ', quotechar=',', quoting=csv.QUOTE_MINIMAL)
    b = [_ for _ in films]
    for key,value in film_1.items():
        csvfile.writerow([key,value])
        """

writer = csv.writer(open('file.csv', 'wb'))
for film in films:
    for key, value in film_1.items():
        writer.writerow(['{},{}'.format(key,value)])