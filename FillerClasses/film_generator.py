import names
producer_name_generator = (names.get_full_name() for _ in range(10))

#Create training set as set of attributes: Title, Article size (in symbols), Producer, Release date, Running time,
#Country. Use budget as prediction value.
#As result there should be function which predicts budget by set of other parameters (Title, Article size (in symbols),
#Producer, Release date, Running time, Country).

#generator for producer name generator

#producer_name_ = (str(item) for item in producer_name_generator)

title = list()
article_size = list()

producer = {
    "name": producer_name_generator,
    "film": "Titanic"
}
release_date = list()
running_time = list()
country = list()

x_budget = list()

"""
>>>
>>> names.get_full_name()
u'Patricia Halford'
>>> names.get_full_name(gender='male')
u'Patrick Keating'
>>> names.get_first_name()
'Bernard'
>>> names.get_first_name(gender='female')
'Christina'
>>> names.get_last_name()
'Szczepanek'
"""

for key, val in producer.items():
    print '{} - {}'.format(key, val)