__author__ = 'Oleg.Koval'
"""
Note:
Goal: to implement film budget prediction on Python (PyBrain library) using simple neural network
Technologies: Supervised machine learning (linear regression) with pybrain (back-Propagation neural network)
"""
#
#imports
#

#for dataset, dataset=training set
from pybrain.datasets import SupervisedDataSet
#for network
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.tools.shortcuts import buildNetwork
#for XML



file_csv = '{}'.format('../FillerClasses/file.csv')

with open(file_csv) as f:
    content = f.readlines()

#creating dataset
n_inputs = 7
n_outputs = 1
n_hidden = 3

my_data_set = SupervisedDataSet(n_inputs, n_outputs)

#creating network
sample_LAN = buildNetwork(n_inputs, n_outputs, n_hidden, bias = True, reccurent = True)

#load dataset

my_data_set = SupervisedDataSet.loadFromFile(file_csv)

#train network
trainer = BackpropTrainer(self.net, learningrate=0.01, lrdecay=1, momentum=0.0, verbose=True)
trainer.trainUntilConvergence(my_data_set, verbose=True)